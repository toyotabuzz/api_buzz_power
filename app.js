var express = require("express");
var app = express();

const db = require('./config/db');

db.authenticate()
  .then(()=>console.log("Database connect."))
  .catch((err)=>console.log(err));

app.get("/", (req, res) => {
  res.end("App Start");
});

var server = app.listen(3000, () => {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Server Start..", host, port);
});
