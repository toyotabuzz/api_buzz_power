const Sequelize = require('sequelize', {
    host: 'root',
    dialect: 'mysql',
    directory: false, // prevents the program from writing to disk
    port: '3306',
});

const connect = new Sequelize('buzz_power', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    operatorsAliases :false,
});

module.exports = connect;